import { createSwear } from '@swear-js/react';
import { api } from '../api';

const defaultState: { data: Record<string, any | any[]>, loading: boolean } = {
  data: {},
  loading: false,
};

export const seanceSwear = createSwear('seance', defaultState, (mutate) => ({
  fetchSeance: (id: string) => {
    mutate({ ...defaultState, loading: true });
    api.get(`/api/seance/${id}`)
      .then((res) => {
        mutate({ data: res.data, loading: false });
      })
      .catch(() => mutate({ ...defaultState }));
  },
}));
