import { createSwear } from '@swear-js/react';
import axios from 'axios';
import { api } from '../api';

type stateTypes = {
  data: ReadonlyArray<any>;
  loading: boolean;
};

const defaultState: stateTypes = {
  data: [],
  loading: false,
};

export const filmsSwear = createSwear('posts', defaultState, (mutate) => ({
  fetchFilms: () => {
    mutate({ ...defaultState, loading: true });
    api.get('/api/films')
      .then((res) => res.data)
      .then((data) => {
        mutate({ ...defaultState, data });
      })
      .catch(() => mutate({ ...defaultState }));
  },
}));
