import { createSwear } from '@swear-js/react';
import { api } from '../api';

const defaultState: Record<string, any> = {};

export const userSwear = createSwear('user', defaultState, (mutate) => ({
  fetchUser: () => {
    api.get('/api/auth/user')
      .then((res) => {
        mutate(res.data);
      })
      .catch(() => alert('User error'));
  },
}));
