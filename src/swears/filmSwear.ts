import { createSwear } from '@swear-js/react';
import axios from 'axios';
import { api } from '../api';

type stateTypes = {
  data: any;
  loading: boolean;
};

const defaultState: stateTypes = {
  data: {},
  loading: false,
};

export const filmSwear = createSwear('post', defaultState, (mutate) => ({
  fetchFilm: (id: string) => {
    mutate({ ...defaultState, loading: true });
    api.get(`/api/film/${id}`)
      .then((res) => res.data)
      .then((data) => {
        mutate({ ...defaultState, data });
      })
      .catch((e) => {
        mutate({ ...defaultState });
      });
  },
}));
