import React, {
  FC, useEffect, useState,
} from 'react';
import { useSwear } from '@swear-js/react';

import { Link } from 'react-router-dom';
import { MoviePreview } from '../components/MoviePreview';
import { Typography } from '../components/Typography';
import { colors } from '../core';
import { filmsSwear } from '../swears/filmsSwear';

export const FeedPage: FC = () => {
  const [{ data, loading }, { fetchFilms }] = useSwear(filmsSwear);

  useEffect(() => {
    fetchFilms();
  }, []);

  return (
    <div>
      {loading ? <div>Загрузка</div>
        : (
          <>
            {data.length ? (
              <div
                style={{
                  backgroundImage: `linear-gradient(rgba(0,0,0,.6), rgba(0,0,0,.6)), url(${data[0]?.promoImage ?? data[0]?.image})`,
                  position: 'relative',
                }}
              >
                <div
                  style={{
                    position: 'sticky',
                    display: 'flex',
                    justifyContent: 'space-between',
                    marginBottom: 38,
                    top: 0,
                    padding: '10px 25px',
                    paddingTop: 30,
                    zIndex: 9999,
                    backdropFilter: 'blur(4px)',
                  }}
                >
                  <Typography
                    style={{
                      color: colors.Brand.White,
                      whiteSpace: 'pre-line',
                      margin: 0,
                    }}
                    as="h2"
                    size={24}
                  >
                    Киносессии
                    {'\n'}
                    на сегодня
                  </Typography>
                  <Typography
                    style={{
                      color: colors.Brand.White,
                      margin: 0,
                    }}
                    as="h2"
                    size={24}
                  >
                    <Link
                      to="/profile"
                      style={{
                        textDecoration: 'none',
                        color: 'inherit',
                      }}
                    >
                      Профиль
                    </Link>
                  </Typography>
                </div>
                <MoviePreview key={data[0]?.title} data={data[0]} buttonTitle="Сеансы" isTop />
                <div
                  style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    width: '100%',
                    height: '100%',
                    zIndex: 10,
                    backdropFilter: 'grayscale(100%)',
                  }}
                />
              </div>
            ) : null}

            <div
              style={{
                marginTop: 50,
                display: 'grid',
                gap: 50,
                padding: 25,
              }}
            >
              {data.map((d) => <MoviePreview key={d.title} data={d} buttonTitle="Сеансы" />)}
            </div>
          </>
        )}
    </div>
  );
};
