import React, {
  FC, useCallback, useContext, useEffect, useMemo, useRef, useState,
} from 'react';
import { useSwear } from '@swear-js/react';
import { useParams } from 'react-router';
import { Link } from 'react-router-dom';
import Hls from 'hls.js';
import { seanceSwear } from '../swears/seanceSwear';
import { SocketContext } from '../contexts/SocketContext';
import { Button } from '../components/Button';
import { api } from '../api';
import { userSwear } from '../swears/userSwear';
import { MessageCloud } from '../components/MessageCloud';

export const SeancePage: FC = () => {
  const params = useParams<{ id: string }>();
  const socket = useContext(SocketContext);
  const [{ data: seance }, { fetchSeance, set: setSeance }] = useSwear(seanceSwear);
  const [user, { fetchUser }] = useSwear(userSwear);
  const messages = useMemo(() => seance?.messages || [], [seance]);
  const [content, setContent] = useState('');
  const messageContainerRef = useRef<HTMLDivElement>(null);
  const playerRef = useRef<HTMLVideoElement>(null);
  const [playing, setPlaying] = useState(false);

  const videoSrc = useMemo(() => `http://localhost:3003/streams/${seance?.film?.filmFile}.m3u8`, [seance]);

  useEffect(() => {
    fetchSeance(params.id);
    fetchUser();
    socket?.emit('auth', { token: localStorage.getItem('token'), seanceId: params.id });
  }, []);

  useEffect(() => {
    if (seance?.datetime) {
      socket?.on('ON_MESSAGE', (message) => {
        setSeance({
          data: {
            ...seance,
            messages: [
              ...seance.messages,
              message,
            ],
          },
          loading: false,
        });
      });
    }
  }, [seance, setSeance]);

  const handleMessageSend = useCallback(() => {
    if (content.length) {
      api.post(`/api/message/send/${params.id}`, { content, datetime: (new Date(Date.now())) })
        .then(() => {
          setContent('');
        })
        .catch(() => {
          alert('Something went wrong');
        });
    }
  }, [content]);

  const timeDifference = useMemo(() => {
    const seanceDatetime = new Date(seance.datetime);
    const nowDatetime = new Date(Date.now());
    // @ts-ignore
    return seanceDatetime - nowDatetime;
  }, [seance]);

  useEffect(() => {
    if (timeDifference > 0) {
      setTimeout(() => fetchSeance(params.id), timeDifference);
    }
  }, [timeDifference, seance]);

  const loadVideo = useCallback(() => {
    if (seance?.film && playerRef.current) {
      const video = playerRef.current;
      if (Hls.isSupported()) {
        const hls = new Hls();

        hls.loadSource(videoSrc);
        hls.attachMedia(video);
        hls.on(Hls.Events.MANIFEST_PARSED, () => {
          video.currentTime = Math.abs(timeDifference) / 1000;
          video.play();
          video.addEventListener('ended', (event) => {
            window.location.reload();
          });
        });
      } else if (playerRef.current.canPlayType('application/vnd.apple.mpegurl')) {
        video.src = videoSrc;
        video.addEventListener('loadedmetadata', () => {
          video.currentTime = Math.abs(timeDifference) / 1000;
          video.play();
        });
        video.addEventListener('ended', (event) => {
          window.location.reload();
        });
      }
    }
  }, [seance, playerRef]);

  const isTitleBig = useMemo(() => seance.film?.title.length > 15, [seance]);

  useEffect(() => {
    if (messageContainerRef.current) {
      messageContainerRef.current.scrollTop = messageContainerRef.current.scrollHeight;
    }
  }, [messages]);

  const formatByZero = (datePart: number) => {
    const str = `${datePart}`;
    if (str.length === 2) {
      return str;
    }
    return `0${str}`;
  };

  return (
    <div style={{ color: '#fff', height: '100vh' }}>
      <div style={{ display: 'flex', flexDirection: 'column', height: '100%' }}>
        <div style={{ background: 'rgba(255,255,255,.05)', aspectRatio: '18 / 9', position: 'relative' }}>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              padding: 25,
              alignItems: 'center',
              backgroundImage: 'linear-gradient(rgb(0, 0, 0) 22%, transparent 78%)',
              position: 'absolute',
              top: 0,
              left: 0,
              width: '100%',
              zIndex: 10,
            }}
          >
            <Link to="/profile/seances">
              <svg width="31" height="16" viewBox="0 0 31 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0.292891 7.29289C-0.0976334 7.68342 -0.0976334 8.31658 0.292891 8.70711L6.65685 15.0711C7.04738 15.4616 7.68054 15.4616 8.07107 15.0711C8.46159 14.6805 8.46159 14.0474 8.07107 13.6569L2.41421 8L8.07107 2.34315C8.46159 1.95262 8.46159 1.31946 8.07107 0.928932C7.68054 0.538408 7.04738 0.538408 6.65685 0.928932L0.292891 7.29289ZM31 7L0.999998 7V9L31 9V7Z" fill="white" />
              </svg>
            </Link>
            <h3
              style={{
                fontSize: isTitleBig ? 16 : 24,
                margin: 0,
                textTransform: isTitleBig ? 'initial' : 'uppercase',
              }}
            >
              {seance.film?.title}
            </h3>
          </div>
          {seance?.datetime && timeDifference > 0 ? (
            <div
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: '100%',
                background: 'rgba(0,0,0,.8)',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                textAlign: 'center',
              }}
            >
              <div>
                <span style={{ fontSize: 11, fontWeight: 500 }}>До начала сеанса осталось</span>
                <div style={{ fontSize: 38, fontWeight: 'bold' }}>
                  {formatByZero(Math.floor((timeDifference / (1000 * 60 * 60)) % 24))}
                  :
                  {formatByZero(Math.ceil((timeDifference / (1000 * 60)) % 60))}
                </div>
              </div>
            </div>
          ) : null}
          {seance?.datetime && timeDifference < 0 && Math.abs(timeDifference) > (seance.film.duration * 1000) ? (
            <div
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: '100%',
                background: 'rgba(0,0,0,.8)',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                textAlign: 'center',
              }}
            >
              <div>
                <span style={{ fontSize: 11, fontWeight: 500 }}>Сеанс завершен</span>
                <div style={{ fontSize: 38, fontWeight: 'bold' }}>
                  Спасибо!
                </div>
              </div>
            </div>
          ) : null}
          {seance?.datetime && timeDifference < 0 && Math.abs(timeDifference) < (seance.film.duration * 1000) && !playing ? (
            <button
              style={{
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: '100%',
                background: 'rgba(0,0,0,.8)',
                border: 'none',
                outline: 'none',
                color: '#fff',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                textAlign: 'center',
                zIndex: 5,
              }}
              onClick={() => {
                loadVideo();
                setPlaying(true);
              }}
            >
              <span>▶ Начать просмотр</span>
            </button>
          ) : null}
          {/* eslint-disable-next-line jsx-a11y/media-has-caption */}
          <video
            ref={playerRef}
            style={{
              display: 'block',
              height: '100%',
              width: '100%',
              backgroundImage: `url(${seance?.film?.image}), linear-gradient(to top, rgba(0,0,0,.7), rgba(0,0,0,.7)), url(${seance?.film?.promoImage ?? seance?.film?.image})`,
              backgroundSize: 'auto 100%, cover, cover',
              backgroundRepeat: 'no-repeat',
              backgroundPosition: 'center',
            }}
          />
        </div>
        <div
          style={{
            padding: '20px 25px',
            borderBottom: '1px solid rgba(255,255,255,.08)',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <h3 style={{ margin: 0, fontWeight: 'bold' }}>Чат</h3>
        </div>
        <div
          ref={messageContainerRef}
          style={{
            flex: 1,
            overflow: 'scroll',
            padding: '20px 25px',
          }}
        >
          {messages.map((n: any) => (
            <div
              key={n._id}
              style={{
                display: 'flex',
                padding: '5px 0',
                justifyContent: n.user._id === user._id ? 'flex-end' : 'flex-start',
              }}
            >
              <MessageCloud message={n} isMe={n.user._id === user._id} />
            </div>
          ))}
        </div>
        <div
          style={{
            display: 'flex',
            alignItems: 'flex-start',
            gap: 20,
            padding: '10px 20px',
          }}
        >
          <textarea
            style={{
              flex: 1,
              background: '#101010',
              color: '#fff',
              borderRadius: 25,
              border: 'none',
              padding: 20,
              resize: 'none',
              fontSize: 13,
              outline: 'none',
              fontFamily: 'inherit',
            }}
            placeholder="Начинайте набирать сообщение"
            value={content}
            onChange={(e) => setContent(e.currentTarget.value)}
          />
          <button
            style={{
              background: '#fff',
              borderRadius: 40,
              cursor: 'pointer',
              outline: 'none',
              border: 'none',
              width: 40,
              height: 40,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onClick={handleMessageSend}
          >
            <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M8.70711 0.292893C8.31658 -0.0976315 7.68342 -0.0976314 7.29289 0.292893L0.928932 6.65686C0.538407 7.04738 0.538407 7.68054 0.928932 8.07107C1.31946 8.46159 1.95262 8.46159 2.34315 8.07107L8 2.41421L13.6569 8.07107C14.0474 8.46159 14.6805 8.46159 15.0711 8.07107C15.4616 7.68054 15.4616 7.04738 15.0711 6.65685L8.70711 0.292893ZM9 15L9 1L7 1L7 15L9 15Z" fill="#F10774" />
            </svg>
          </button>
        </div>
      </div>
    </div>
  );
};
