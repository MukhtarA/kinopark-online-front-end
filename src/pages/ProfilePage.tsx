import React, {
  FC, useCallback, useEffect, useMemo, useState,
} from 'react';
import { useSwear } from '@swear-js/react';

import { Link } from 'react-router-dom';
import { MoviePreview } from '../components/MoviePreview';
import { Typography } from '../components/Typography';
import { colors } from '../core';
import { userSwear } from '../swears/userSwear';
import { CircleButton } from '../components/CircleButton';
import { api } from '../api';

export const ProfilePage: FC = () => {
  const [user, { fetchUser }] = useSwear(userSwear);

  useEffect(() => {
    if (localStorage.getItem('token')) fetchUser();
  }, []);

  const films = useMemo(() => {
    if (user.seances) {
      return user.seances.reduce((acc: any[], curr: any) => (acc.includes(curr.film) ? acc : [...acc, curr.film]), []);
    }
    return [];
  }, [user]);

  const logout = useCallback(() => {
    api.post('/api/auth/logout', { token: localStorage.getItem('token') })
      .then(() => {
        localStorage.removeItem('token');
        window.location.reload();
      });
  }, []);

  const formatByZero = (datePart: number) => {
    const str = `${datePart}`;
    if (str.length === 2) {
      return str;
    }
    return `0${str}`;
  };

  const formatDate = (date: string) => {
    const parsed = new Date(date);
    return `${formatByZero(parsed.getDay())}.${formatByZero(parsed.getMonth())}.${parsed.getFullYear()} ${formatByZero(parsed.getHours())}:${formatByZero(parsed.getMinutes())}`;
  };

  const getDifference = (datetime: string) => {
    const seanceDatetime = new Date(datetime);
    const nowDatetime = new Date(Date.now());
    // @ts-ignore
    return seanceDatetime - nowDatetime;
  };

  return (
    <div style={{ padding: 25 }}>
      <div
        style={{
          position: 'sticky',
          display: 'flex',
          justifyContent: 'space-between',
          marginBottom: 38,
          top: 0,
          padding: '10px 25px',
          paddingTop: 30,
          zIndex: 9999,
          backdropFilter: 'blur(4px)',
        }}
      >
        <Typography
          style={{
            color: colors.Brand.Sand,
            whiteSpace: 'pre-line',
            margin: 0,
            textTransform: 'uppercase',
          }}
          as="h2"
          size={26}
        >
          <Link
            to="/feed"
            style={{
              textDecoration: 'none',
              color: 'inherit',
            }}
          >
            Назад
          </Link>
        </Typography>
        <Typography
          style={{
            color: colors.Brand.White,
            margin: 0,
          }}
          as="h2"
          size={24}
        >
          Профиль
        </Typography>
      </div>

      <div
        style={{
          display: 'grid',
          gap: 50,
          padding: 25,
        }}
      >
        {user.seances?.map((d: any) => (
          <div style={{ borderBottom: '1px solid rgba(255,255,255,.2)', padding: '20px 0' }} key={d._id}>
            <MoviePreview key={d._id} data={d.film} buttonTitle="Перейти" bought seanceId={d._id} expired={getDifference(d.datetime) < 0} />
            <p style={{
              display: 'flex', gap: 10, margin: 0, marginTop: 20,
            }}
            >
              <span>Время:</span>
              <span>{formatDate(d.datetime)}</span>
            </p>
            <p style={{ display: 'flex', gap: 10, margin: 0 }}>
              <span>Оплачено:</span>
              <span>
                {d.price}
                тг
              </span>
            </p>
          </div>
        ))}
        {films.length === 0 ? (
          <p
            style={{
              fontSize: 18,
              color: '#d7d7d7',
              textAlign: 'center',
            }}
          >
            Вы еще не купили билет
          </p>
        ) : null}
      </div>

      <div style={{ textAlign: 'center', marginTop: 50 }}>
        <CircleButton
          onClick={logout}
          style={{
            background: 'red',
            color: '#fff',
            fontSize: 18,
            border: 'none',
            fontWeight: 'bold',
            padding: '10px 25px',
            borderRadius: 30,
            textTransform: 'uppercase',
          }}
        >
          Выйти
        </CircleButton>
      </div>

    </div>
  );
};
