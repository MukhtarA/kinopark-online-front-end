import React, {
  FC, useEffect,
} from 'react';

import { Link } from 'react-router-dom';
import { useHistory } from 'react-router';
import image from '../assets/img.png';
import logo from '../assets/logo.png';

export const MainPage: FC = () => {
  const history = useHistory();
  useEffect(() => {
    if (localStorage.getItem('token')) {
      history.push('/feed');
    }
  }, []);
  return (
    <div
      style={{
        position: 'relative',
      }}
    >
      <img
        src={image}
        alt="kinopark"
        style={{
          width: '100%',
        }}
      />
      <div style={{ padding: 25, position: 'relative' }}>
        <div style={{ fontSize: 45, lineHeight: 1, fontWeight: 'bold' }}>
          <p style={{ margin: 0, color: '#F10774' }}>KINOPARK</p>
          <p style={{ margin: 0, color: '#FFD9AC' }}>ONLINE</p>
          <p style={{ margin: 0 }}>CINEMA</p>
        </div>
        <img
          src={logo}
          alt="logo"
          style={{
            position: 'absolute',
            right: 0,
            height: 120,
            top: 100,
          }}
        />
        <div style={{ marginTop: 100, textAlign: 'right', fontSize: 35 }}>
          <Link to="/login" style={{ textDecoration: 'none', color: '#FFF', fontWeight: 'bold' }}>ВОЙТИ</Link>
          <br />
          <Link
            to="/register"
            style={{
              textDecoration: 'none', color: '#FFD9AC', fontWeight: 'bold', fontSize: 30, display: 'inline-block',
            }}
          >
            РЕГИСТРАЦИЯ
          </Link>
        </div>
      </div>

    </div>
  );
};
