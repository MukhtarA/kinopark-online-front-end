import React, {
  FC, useCallback, useEffect, useState,
} from 'react';
import { useHistory } from 'react-router';
import { api } from '../api';
import { CircleButton } from '../components/CircleButton';
import { colors } from '../core';

export const RegisterPage: FC = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState<string | null>(null);
  const history = useHistory();

  const auth = useCallback(() => {
    api.post('/api/auth/register', { email, password, name })
      .then((res) => {
        localStorage?.setItem('token', res.data.session?.token);
        window.location.reload();
      })
      .catch(() => {
        setError('Проверьте правильность заполненных данных!');
      });
  }, [email, password]);

  useEffect(() => {
    if (localStorage.getItem('token')) {
      history.push('/');
    }
  }, []);

  return (
    <div style={{ padding: '45px 25px' }}>
      <div>
        <button
          style={{
            background: 'transparent',
            cursor: 'pointer',
            border: 'none',
            outline: 'none',
          }}
          onClick={() => history.push('/')}
        >
          <svg width="79" height="54" viewBox="0 0 79 54" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="52" cy="27" r="27" fill="#1D1D1D" />
            <path d="M0.585785 16.5858C-0.195263 17.3668 -0.195263 18.6332 0.585785 19.4142L13.3137 32.1421C14.0948 32.9232 15.3611 32.9232 16.1421 32.1421C16.9232 31.3611 16.9232 30.0948 16.1421 29.3137L4.82843 18L16.1421 6.6863C16.9232 5.90525 16.9232 4.63892 16.1421 3.85787C15.3611 3.07682 14.0948 3.07682 13.3137 3.85787L0.585785 16.5858ZM48 16L2 16L2 20L48 20L48 16Z" fill="white" />
          </svg>
        </button>
      </div>
      <div style={{ padding: 20, paddingTop: 150 }}>
        <h1 style={{ color: '#FFD9AC' }}>РЕГИСТРАЦИЯ</h1>
        {error ? (
          <div
            style={{
              color: 'red',
              textTransform: 'uppercase',
              fontWeight: 'bold',
              lineHeight: 1.2,
              marginBottom: 30,
            }}
          >
            {error}
          </div>
        ) : null}
        <input
          type="text"
          value={name}
          onChange={(e) => {
            setError(null);
            setName(e.currentTarget.value);
          }}
          placeholder="Ваше имя"
          style={{
            fontSize: 20,
            background: 'transparent',
            border: 'none',
            padding: 0,
            fontFamily: 'inherit',
            textTransform: 'uppercase',
            outline: 'none',
            fontWeight: 'bold',
            color: '#fff',
            width: '100%',
            marginBottom: 10,
          }}
        />
        <input
          type="text"
          value={email}
          onChange={(e) => {
            setError(null);
            setEmail(e.currentTarget.value);
          }}
          placeholder="Ваша почта"
          style={{
            fontSize: 20,
            background: 'transparent',
            border: 'none',
            padding: 0,
            fontFamily: 'inherit',
            textTransform: 'uppercase',
            outline: 'none',
            fontWeight: 'bold',
            color: '#fff',
            width: '100%',
            marginBottom: 10,
          }}
        />
        <input
          type="password"
          value={password}
          onChange={(e) => {
            setError(null);
            setPassword(e.currentTarget.value);
          }}
          placeholder="Пароль"
          style={{
            fontSize: 20,
            background: 'transparent',
            border: 'none',
            padding: 0,
            fontFamily: 'inherit',
            textTransform: 'uppercase',
            outline: 'none',
            fontWeight: 'bold',
            color: '#fff',
            width: '100%',
            marginBottom: 15,
          }}
        />
        <div style={{ marginTop: 50, textAlign: 'right' }}>
          <CircleButton
            onClick={auth}
            style={{
              background: colors.Brand.Purple,
              color: '#fff',
              fontSize: 18,
              border: 'none',
              fontWeight: 'bold',
              padding: '10px 25px',
              borderRadius: 30,
              textTransform: 'uppercase',
            }}
          >
            Зарегистрироваться
          </CircleButton>
        </div>
      </div>
    </div>
  );
};
