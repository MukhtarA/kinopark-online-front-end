import React, {
  FC, MouseEventHandler, useCallback, useEffect, useMemo, useState,
} from 'react';
import { useSwear } from '@swear-js/react';

import { Link } from 'react-router-dom';
import { useHistory, useParams } from 'react-router';
import { filmSwear } from '../swears/filmSwear';
import { Typography } from '../components/Typography';
import { MoviePreview } from '../components/MoviePreview';
import { colors } from '../core';
import { CircleButton } from '../components/CircleButton';
import { api } from '../api';

export const FilmPage: FC = () => {
  const [{ data: film, loading }, { fetchFilm }] = useSwear(filmSwear);
  const [checked, setChecked] = useState<string | null>(null);
  const params = useParams<{ id: string }>();
  const history = useHistory();

  useEffect(() => {
    if (params.id) fetchFilm(params.id);
  }, []);

  const formatByZero = (datePart: number) => {
    const str = `${datePart}`;
    if (str.length === 2) {
      return str;
    }
    return `0${str}`;
  };

  const formatTime = (date: string) => {
    const parsed = new Date(date);
    return `${formatByZero(parsed.getHours())}:${formatByZero(parsed.getMinutes())}`;
  };

  const getMonthName = (month: number) => {
    const months = ['января', 'декабря', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
    return months[month - 1];
  };

  const formatDate = (date: string) => {
    const parsed = new Date(date);
    return `${parsed.getDay()} ${getMonthName(parsed.getMonth())}`;
  };

  const purchase = useCallback((id: string) => (e: { stopPropagation: () => void; }) => {
    e.stopPropagation();
    api.post(`/api/seance/purchase/${id}`)
      .then((res) => {
        history.push('/profile');
      });
  }, []);

  const getDifference = (datetime: string) => {
    const seanceDatetime = new Date(datetime);
    const nowDatetime = new Date(Date.now());
    // @ts-ignore
    return seanceDatetime - nowDatetime;
  };

  const seances = useMemo(() => {
    if (film?.seances) {
      return film.seances.filter((n: any) => getDifference(n.datetime) > 0);
    } return [];
  }, [film]);

  return loading ? <span>Загрузка...</span> : (
    <div>
      <div
        style={{
          backgroundImage: `linear-gradient(rgba(0,0,0,.6), rgba(0,0,0,.6)), url(${film?.promoImage ?? film?.image})`,
          position: 'relative',
        }}
      >
        <div
          style={{
            position: 'sticky',
            display: 'flex',
            justifyContent: 'space-between',
            marginBottom: 38,
            top: 0,
            padding: '10px 25px',
            paddingTop: 30,
            zIndex: 9999,
            backdropFilter: 'blur(4px)',
          }}
        >
          <Typography
            style={{
              color: colors.Brand.Sand,
              whiteSpace: 'pre-line',
              margin: 0,
            }}
            as="h2"
            size={24}
          >
            Сеансы
          </Typography>
          <Typography
            style={{
              color: colors.Brand.White,
              margin: 0,
            }}
            as="h2"
            size={24}
          >
            <Link
              to="/profile"
              style={{
                textDecoration: 'none',
                color: 'inherit',
              }}
            >
              Профиль
            </Link>
          </Typography>
        </div>
        <MoviePreview key={film?.title} data={film} isTop />
        <div
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            zIndex: 10,
            backdropFilter: 'grayscale(100%)',
          }}
        />
      </div>
      <div style={{ padding: '40px 25px' }}>
        <h3 style={{ margin: 0, textTransform: 'uppercase' }}>Выберите сеанс</h3>
      </div>
      <div>
        {seances.map((n: any) => (
          <button
            key={n._id}
            style={{
              background: 'transparent',
              border: 'none',
              outline: 'none',
              display: 'block',
              width: '100%',
              color: '#fff',
              textAlign: 'left',
              padding: '25px',
              borderBottom: '1px solid rgba(255,255,255,.2)',
              fontFamily: 'inherit',
              borderLeft: checked === n._id ? '5px solid #F10774' : 'none',
            }}
            onClick={() => setChecked(n._id)}
          >
            <span
              style={{
                fontSize: 90,
                fontWeight: 'bold',
              }}
            >
              {formatTime(n.datetime)}
            </span>
            <p
              style={{
                color: colors.Brand.Sand,
                fontSize: 24,
                fontWeight: 500,
                padding: '0 15px',
                marginTop: 0,
                textTransform: 'uppercase',
              }}
            >
              {formatDate(n.datetime)}
            </p>
            {checked === n._id ? (
              <div>
                <CircleButton
                  onClick={purchase(n._id)}
                  style={{
                    background: colors.Brand.Purple,
                    color: '#fff',
                    fontSize: 18,
                    border: 'none',
                    fontWeight: 'bold',
                    padding: '10px 25px',
                    borderRadius: 30,
                    textTransform: 'uppercase',
                  }}
                >
                  Оформить билет
                </CircleButton>
              </div>
            ) : null}
          </button>
        ))}
        {seances.length === 0 ? (
          <p
            style={{
              fontSize: 18,
              color: '#d7d7d7',
              textAlign: 'center',
            }}
          >
            На данный фильм нет сеансов
          </p>
        ) : null}
      </div>
    </div>
  );
};
