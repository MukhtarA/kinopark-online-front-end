import React, { useEffect } from 'react';
import { createStore } from '@swear-js/core';
import { swearContext } from '@swear-js/react';
import { swearLogger } from '@swear-js/logger';
import { Route, Switch, useLocation } from 'react-router-dom';
import { io } from 'socket.io-client';
import { useHistory } from 'react-router';
import { MainPage } from './pages/MainPage';
import { FilmPage } from './pages/FilmPage';
import { FeedPage } from './pages/FeedPage';
import { SocketContext } from './contexts/SocketContext';
import { SeancePage } from './pages/SeancePage';
import { LoginPage } from './pages/LoginPage';
import { RegisterPage } from './pages/RegisterPage';
import { ProfilePage } from './pages/ProfilePage';

const socket = io(process.env.REACT_APP_SOCKET_URL || 'localhost:4000');

function App() {
  const store = createStore({ onPatch: swearLogger() });
  const history = useHistory();
  useEffect(() => {
    if (!localStorage.getItem('token')) {
      history.push('/');
    }
  }, []);
  return (
    <div style={{
      fontFamily: 'Inter',
    }}
    >
      <SocketContext.Provider value={socket}>
        <swearContext.Provider value={store}>
          <Switch>
            <Route path="/film/:id">
              <FilmPage />
            </Route>
            <Route path="/feed">
              <FeedPage />
            </Route>
            <Route path="/login">
              <LoginPage />
            </Route>
            <Route path="/register">
              <RegisterPage />
            </Route>
            <Route path="/profile">
              <ProfilePage />
            </Route>
            <Route path="/seance/:id">
              <SeancePage />
            </Route>
            <Route path="/">
              <MainPage />
            </Route>
          </Switch>
        </swearContext.Provider>
      </SocketContext.Provider>
    </div>

  );
}

export default App;
