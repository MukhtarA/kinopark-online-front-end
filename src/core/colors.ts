// eslint-disable-next-line no-shadow
export enum Brand {
  Sand = '#FFD9AC',
  Purple = '#F10774',
  White = '#FFF',
  Error = '#FF3333',
  RedIndicator = '#FF3333',
  Black = '#000000',
}

// eslint-disable-next-line no-shadow
export enum Background {
  White = '#FFF',
  Overlay = 'rgba(0, 0, 0, 0.5)',
  Transparent = 'transparent',
}

// eslint-disable-next-line no-shadow
export enum Text {
  Light = '#FFF',
  Dark = '#282828',
  MainBlack = '#0F0E1E',
  GreySecondary = '#BCBCBC',
  GreySlight = '#dddddd',
  DarkGrey = '#8E8E8E',
}

export type ColorProp = Brand | Background | Text;
