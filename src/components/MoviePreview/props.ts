import { HTMLAttributes, ReactNode } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  readonly data: any;
  readonly isTop?: boolean;
  readonly buttonTitle?: string
  readonly bought?: boolean;
  readonly seanceId?: string;
  readonly expired?: boolean;
};
