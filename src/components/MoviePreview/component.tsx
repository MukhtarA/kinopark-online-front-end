import styled from '@emotion/styled';
import { css } from '@emotion/css';
import React, { FC } from 'react';

import { useHistory } from 'react-router';
import { Props } from './props';
import { Typography } from '../Typography';
import { colors } from '../../core';

export const MoviePreview: FC<Props> = ({
  data, buttonTitle, isTop, bought, seanceId, expired, ...rest
}: Props) => {
  const history = useHistory();
  return (
    <div
      style={{
        display: 'flex', gap: 35, padding: isTop ? 25 : 0, paddingTop: 0, paddingBottom: isTop ? 50 : 0, zIndex: 30, position: 'relative',
      }}
      {...rest}
    >
      <img style={{ maxWidth: 120, maxHeight: 182, borderRadius: 21 }} alt="logo" src={data.image} />
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <Typography
          as="h2"
          color={colors.Brand.White}
          size={24}
          style={{
            padding: '5px 0',
            margin: 0,
            color: colors.Brand.White,
            textTransform: 'uppercase',
          }}
        >
          {data.title}
        </Typography>
        <Typography as="h4" color={colors.Brand.Sand} size={15} style={{ paddingBottom: '8px', margin: 0, color: colors.Brand.Sand }}>
          {data.director}
        </Typography>
        <Typography color={colors.Brand.White} as="p" size={14} style={{ paddingBottom: 25, margin: 0, color: colors.Brand.White }}>
          {data.description}
        </Typography>
        {buttonTitle ? (
          <button
            style={{
              border: 'none',
              outline: 'none',
              borderRadius: 22,
              width: 'fit-content',
              padding: '8px 15px',
              fontSize: 18,
              fontWeight: 'bold',
              textTransform: 'uppercase',
              color: isTop ? colors.Brand.White : colors.Brand.Black,
              // eslint-disable-next-line no-nested-ternary
              backgroundColor: expired ? 'grey' : (isTop ? colors.Brand.Purple : colors.Brand.White),
              cursor: 'pointer',
              display: 'flex',
              gap: 5,
              alignItems: 'center',
            }}
            onClick={!expired ? () => history.push(bought ? `/seance/${seanceId}` : `/film/${data._id}`) : () => null}
          >
            {isTop ? (
              <span>
                <svg width="21" height="17" viewBox="0 0 21 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M10.3125 0.206894L12.6278 6.28278H20.1203L14.0587 10.0379L16.374 16.1138L10.3125 12.3587L4.25096 16.1138L6.56627 10.0379L0.504729 6.28278H7.9972L10.3125 0.206894Z" fill="#4D0225" />
                </svg>
              </span>
            ) : null}
            {expired ? 'Завершен' : buttonTitle}
          </button>
        ) : null}
      </div>
    </div>
  );
};
