import styled from '@emotion/styled';
import { css } from '@emotion/css';
import React, { FC } from 'react';

import { Props } from './props';

const CircleButtonBase: FC<Props> = ({ icon, children, ...rest }: Props) => (
  <button {...rest}>{icon || children}</button>
);

export const CircleButton = styled(CircleButtonBase)<Props>`
  ${({ appearance, size }) => css`
      width: ${size || 50}px;
      height: ${size || 50}px;
      border: none;
      cursor: pointer;
      border-radius: ${size || 25}px;
      outline: none;
      box-shadow: 0 0 15px rgba(75, 70, 150, 0.1);
      ${
  appearance === 'primary'
        && css`
          background: #ffff;
          color: #ffff;
          &:hover {
            background: #ffff;
          }
        `
}
      ${
  appearance === 'default'
        && css`
          background: #ffff;
          color: #ffff;
          &:hover {
            border-color: #ffff;
            color: #ffff;
          }
        `
}
      ${
  appearance === 'translucent'
        && css`
          background: rgba(255, 255, 255, 0.3);
          backdrop-filter: blur(8px);
          &:hover {
            border-color: #ffff;
            color: #ffff;
          }
        `
}
    `}
`;

CircleButton.defaultProps = {
  appearance: 'primary',
};
