import React, { FC, HTMLAttributes } from 'react';

type Props = HTMLAttributes<HTMLDivElement> & {
  message: any;
  isMe: boolean;
};

export const MessageCloud: FC<Props> = ({
  message, isMe, style, ...rest
}: Props) => {
  const formatDate = (date: string) => {
    const dateParsed = new Date(date);

    return `${dateParsed.getHours()}:${dateParsed.getMinutes()}`;
  };

  return (
    <div
      style={{
        display: 'inline-block',
        background: isMe ? '#F10774' : '#202020',
        padding: '15px 17px',
        borderRadius: 22,
        minWidth: 170,
        ...style,
      }}
      {...rest}
    >
      <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
        <span style={{ fontSize: 13, fontWeight: 'bold' }}>{message.user?.name}</span>
        <span style={{ fontSize: 11, color: isMe ? '#fff' : '#B6B1B1' }}>{formatDate(message.datetime)}</span>
      </div>
      <div>
        <p
          style={{
            margin: 0,
            marginTop: 5,
            lineHeight: 1.2,
            fontSize: 13,
          }}
        >
          {message.content}
        </p>
      </div>
    </div>
  );
};
