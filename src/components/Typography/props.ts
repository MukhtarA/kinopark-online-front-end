import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLElement> & {
  // eslint-disable-next-line no-undef
  readonly as: keyof JSX.IntrinsicElements;
  readonly size: number;
};
