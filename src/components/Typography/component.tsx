import { css } from '@emotion/css';
import styled from '@emotion/styled';
import { createElement, FC } from 'react';

import { Props } from './props';

const TypographyBase: FC<Props> = ({ as, children, ...rest }: Props) => createElement(as, rest, children);

export const Typography = styled(TypographyBase)<Props>`
  ${({ size }) => css`
    font-size: ${size}px;
    margin: 0 !important;
  `}
`;
